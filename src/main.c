#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdint.h>
#include <wayland-server-core.h>
#include <libinput.h>
#include <wlr/backend.h>
#include <wlr/backend/libinput.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/allocator.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_pointer.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/types/wlr_server_decoration.h>
#include <wlr/types/wlr_output_management_v1.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_xdg_output_v1.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_gamma_control_v1.h>
#include <wlr/types/wlr_data_control_v1.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>


#define cmd(command) execl("/bin/sh", "/bin/sh", "-c", command, (void*)NULL);
#define assert(amog) if (!amog) return;

const int WORKSPACES = 9;

#include "types.h"

#include "view.c"
#include "workspaces.c"
#include "layer.c"
#include "output_management.c"

void fade_animation(struct plugin *plugin) {
    struct waytile_server *server = plugin->server;
    struct timespec time_now;
    float time_diff;
    int duration = 2;
    int direction = 0;
    timespec_get(&time_now, TIME_UTC);
    time_diff = (time_now.tv_nsec -
                ((struct timespec*)plugin->data)->tv_nsec) / 1000000000. +
                (float)(time_now.tv_sec -
                ((struct timespec*)plugin->data)->tv_sec);
    if (!direction) {
    time_diff = (time_diff * -1) + duration;
    time_diff /= duration;
    }
    server->config->background_color[1] = time_diff;
    printf("%f\n", time_diff);
    if (!direction) {
        if (time_diff <= 0.) {
            wl_list_remove(&plugin->link);
            free(plugin);
            return;
        }
    }
    if (direction) {
        if (time_diff >= (float)duration) {
            wl_list_remove(&plugin->link);
            free(plugin);
            return;
        }
    }
}

static void output_destroy(struct wl_listener *listener, void *data) {
    struct waytile_output *output = wl_container_of(listener, output, destroy);
    wl_list_remove(&output->link);
    wl_list_remove(&output->destroy.link);
    wl_list_remove(&output->frame.link);
    free(output);
}

struct render_data {
	struct wlr_output *output;
	struct wlr_renderer *renderer;
	struct waytile_view *view;
	struct timespec *when;
};

static void render_borders(struct wlr_surface *surface, void *data) {
    struct render_data *rdata = data;
    struct waytile_view *view = rdata->view;
    struct waytile_output *output = rdata->output;
    struct wlr_output *wlr_output = output->wlr_output;
	struct waytile_server *server = view->server;
	struct waytile_config *config = server->config;
	int thickness = config->border_thickness;
	float *color;

	if (server->workspaces[output->workspace].focused_view == view) {
		color = &config->border_active_color;
	} else {
		color = &config->border_inactive_color;
	}

    double ox = 0, oy = 0;
    wlr_output_layout_output_coords(view->server->output_layout, wlr_output, &ox, &oy);
    ox += view->x, oy += view->y;

	struct wlr_box box = {
        .x = ox - thickness,
        .y = oy - thickness,
        .width = view->width + thickness,
        .height = thickness,
    };

	wlr_render_rect(server->renderer, &box, color, wlr_output->transform_matrix);

    box.x = ox - thickness;
    box.y = oy;
    box.width = thickness;
    box.height = view->height + thickness;

	wlr_render_rect(server->renderer, &box, color, wlr_output->transform_matrix);

    box.x = ox + view->width;
    box.y = oy - thickness;
    box.width = thickness;
    box.height = view->height + thickness;

	wlr_render_rect(server->renderer, &box, color, wlr_output->transform_matrix);

    box.x = ox;
    box.y = oy + view->height;
    box.width = view->width + thickness;
    box.height = thickness;

	wlr_render_rect(server->renderer, &box, color, wlr_output->transform_matrix);
}

static void render_surface(struct wlr_surface *surface, int sx, int sy, void *data) {
    struct render_data *rdata = data;
    struct waytile_view *view = rdata->view;
    struct wlr_output *output = ((struct waytile_output*)rdata->output)->wlr_output;

    struct wlr_texture *texture = wlr_surface_get_texture(surface);
    assert(texture);

    double ox = 0, oy = 0;
    wlr_output_layout_output_coords(view->server->output_layout, output, &ox, &oy);
    ox += view->x + sx, oy += view->y + sy;

    struct wlr_box box = {
        .x = ox * output->scale,
        .y = oy * output->scale,
        .width = surface->current.width * output->scale,
        .height = surface->current.height * output->scale,
    };

	if (surface->current.width > view->width || surface->current.height > view->height) {
		box.width = view->width;
		box.height = view->width;
	}
    if (surface->current.width != surface->pending.width &&
        surface->current.height != surface->pending.height) {
        return;
    }

    float matrix[9];
	enum wl_output_transform transform = wlr_output_transform_invert(surface->current.transform);
	wlr_matrix_project_box(matrix, &box, transform, 0, output->transform_matrix);

    wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1);
    wlr_surface_send_frame_done(surface, rdata->when);
}

static void output_frame(struct wl_listener *listener, void *data) {
    struct waytile_output *output = wl_container_of(listener, output, frame);
    struct wlr_renderer *renderer = output->server->renderer;

	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

    if (!wlr_output_attach_render(output->wlr_output, NULL)) {
		return;
	}
    int width, height;
    wlr_output_effective_resolution(output->wlr_output, &width, &height);
    wlr_renderer_begin(output->server->renderer, width, height);
    wlr_renderer_clear(output->server->renderer, output->server->config->background_color);

    struct waytile_view *view;
    wl_list_for_each_reverse(view, &output->server->layer_views_background, link) {
        if (!view->mapped) {
            continue;
        }
        struct render_data rdata = {
            .output = output,
            .view = view,
            .renderer = renderer,
            .when = &now,
        };
        wlr_layer_surface_v1_for_each_surface(view->layer_surface,
                render_surface, &rdata);
    }
    wl_list_for_each_reverse(view, &output->server->layer_views_bottom, link) {
        if (!view->mapped) {
            continue;
        }
        struct render_data rdata = {
            .output = output,
            .view = view,
            .renderer = renderer,
            .when = &now,
        };
        wlr_layer_surface_v1_for_each_surface(view->layer_surface,
                render_surface, &rdata);
    }
    if (output->server->workspaces[output->workspace].fullscreen) {
        view = output->server->workspaces[output->workspace].focused_view;
        if (view->mapped) {
            struct render_data rdata = {
                .output = output,
                .view = view,
                .renderer = renderer,
                .when = &now,
            };
            wlr_xdg_surface_for_each_surface(view->xdg_surface, render_surface, &rdata);
        }
    } else {
        wl_list_for_each(view, &output->server->workspaces[output->workspace].views, link) {
            if (!view->mapped) {
                continue;
            }
            struct render_data rdata = {
                .output = output,
                .view = view,
                .renderer = renderer,
                .when = &now,
            };
            wlr_xdg_surface_for_each_surface(view->xdg_surface,
                    render_surface, &rdata);
			render_borders(view->xdg_surface, &rdata);
        }
    }
    wl_list_for_each_reverse(view, &output->server->layer_views_top, link) {
        if (!view->mapped) {
            continue;
        }
        struct render_data rdata = {
            .output = output,
            .view = view,
            .renderer = renderer,
            .when = &now,
        };
        wlr_layer_surface_v1_for_each_surface(view->layer_surface,
                render_surface, &rdata);
    }
    wl_list_for_each_reverse(view, &output->server->layer_views_overlay, link) {
        if (!view->mapped) {
            continue;
        }
        struct render_data rdata = {
            .output = output,
            .view = view,
            .renderer = renderer,
            .when = &now,
        };
        wlr_layer_surface_v1_for_each_surface(view->layer_surface,
                render_surface, &rdata);
    }
    //if (output->server->animation) {
        //void (*anim)(struct waytile_output*) = output->server->animation;
        //anim(output);
    //}

    wlr_output_render_software_cursors(output->wlr_output, NULL);

    wlr_renderer_end(output->server->renderer);
    wlr_output_commit(output->wlr_output);
}


static struct waytile_view *desktop_view_at(struct waytile_server *server, double lx, double ly, struct wlr_surface **surface, double *sx, double *sy) {
    struct waytile_view *view;
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
    struct wlr_surface *_surface = NULL;
    wl_list_for_each_reverse(view, &server->workspaces[output->workspace].views, link) {
        double view_sx = lx - view->x;
        double view_sy = ly - view->y;

        double _sx, _sy;
        _surface = NULL;
        _surface = wlr_xdg_surface_surface_at(view->xdg_surface, view_sx, view_sy, &_sx, &_sy);
        if (_surface) {
            *sx = _sx;
            *sy = _sy;
            *surface = _surface;
            return view;
        }
    }
    return NULL;
}

static void keyboard_handle_modifiers(struct wl_listener *listener, void *data) {
    struct waytile_keyboard *keyboard = wl_container_of(listener, keyboard, modifiers);
    wlr_seat_set_keyboard(keyboard->server->seat, keyboard->device);
    wlr_seat_keyboard_notify_modifiers(keyboard->server->seat, &keyboard->device->keyboard->modifiers);
}

static int handle_keybinds(const xkb_keysym_t sym, uint32_t modifiers, struct waytile_server *server) {
    //TODO: add hotkeys
#if DEBUG
    switch (sym) {
        case XKB_KEY_Escape:
            if (modifiers & WLR_MODIFIER_CTRL) {
                wl_display_terminate(server->wl_display);
            }
            return 0;
            break;
            /*
        case XKB_KEY_bracketleft:
            {
                struct timespec *time_prev = malloc(sizeof(struct timespec));
                timespec_get(time_prev, TIME_UTC);
                server->data = time_prev;
                server->animation = fade_animation;
                break;
            }
            */
        case XKB_KEY_1:
            {
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace < WORKSPACES - 1) {
                    switch_workspace(output, output->workspace + 1);
                }
                break;
            }
        case XKB_KEY_2:
            //TODO account for multiple outputs
            {
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace > 0) {
                    move_view_to_workspace(output, output->workspace - 1);
                    switch_workspace(output, output->workspace - 1);
                }
                break;
            }
        case XKB_KEY_semicolon:
            if (modifiers & WLR_MODIFIER_ALT) {
                if (server->seat->keyboard_state.focused_surface) {
                struct wlr_xdg_surface *surface = wlr_xdg_surface_from_wlr_surface(server->seat->keyboard_state.focused_surface);
                    wlr_xdg_toplevel_send_close(surface);
                }
                break;
            }
            if (modifiers & WLR_MODIFIER_CTRL) {
                if (fork() == 0) {
                    cmd("kitty");
                }
                break;
            }
        default:
            return 0;
    }
    return 1;
#else
    switch (sym) {
        case XKB_KEY_Escape:
            if (modifiers == WLR_MODIFIER_LOGO) {
                wl_display_terminate(server->wl_display);
            }
            if (modifiers == WLR_MODIFIER_ALT) {
                if (server->seat->keyboard_state.focused_surface) {
                struct wlr_xdg_surface *surface = wlr_xdg_surface_from_wlr_surface(server->seat->keyboard_state.focused_surface);
                    wlr_xdg_toplevel_send_close(surface);
                }
                break;
            }
            return 0;
        case XKB_KEY_o:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL)) {
                /*
                double ox = 0, oy = 0;
                int x = 0, y = 0;
                int cx = server->cursor->x, cy = server->cursor->y;
                int width = 0, height = 0;
                struct waytile_output *output;
                wl_list_for_each(output, &server->outputs, link) {
                    //TODO account for scaling
                    wlr_output_layout_output_coords(view->server->output_layout, output, &ox, &oy);
                    x = (int)ox;
                    y = (int)oy;
                    wlr_output_effective_resolution(output->wlr_output, &width, &height);
                    width += x;
                    height += x;
                    if (cx > x && cy > y && cx < width &&
                }
                */
                //TODO account for multiple outputs
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace < WORKSPACES - 1) {
                    switch_workspace(output, output->workspace + 1);
                }
                break;
            }
            return 0;
        case XKB_KEY_n:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL)) {
                //TODO account for multiple outputs
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace > 0) {
                    switch_workspace(output, output->workspace - 1);
                }
                break;
            }
            return 0;
        case XKB_KEY_O:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL | WLR_MODIFIER_SHIFT)) {
                /*
                double ox = 0, oy = 0;
                int x = 0, y = 0;
                int cx = server->cursor->x, cy = server->cursor->y;
                int width = 0, height = 0;
                struct waytile_output *output;
                wl_list_for_each(output, &server->outputs, link) {
                    //TODO account for scaling
                    wlr_output_layout_output_coords(view->server->output_layout, output, &ox, &oy);
                    x = (int)ox;
                    y = (int)oy;
                    wlr_output_effective_resolution(output->wlr_output, &width, &height);
                    width += x;
                    height += x;
                    if (cx > x && cy > y && cx < width &&
                }
                */
                //TODO account for multiple outputs
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace < WORKSPACES - 1) {
                    move_view_to_workspace(output, output->workspace + 1);
                    switch_workspace(output, output->workspace + 1);
                }
                break;
            }
            return 0;
        case XKB_KEY_N:
            printf("%i\n", sym);
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL | WLR_MODIFIER_SHIFT)) {
            printf("%i\n", sym);
                //TODO account for multiple outputs
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace > 0) {
                    move_view_to_workspace(output, output->workspace - 1);
                    switch_workspace(output, output->workspace - 1);
                }
                break;
            }
            return 0;
        case XKB_KEY_e:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL)) {
                //TODO account for multiple outputs
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace < WORKSPACES - 3) {
                    switch_workspace(output, output->workspace + 3);
                }
                break;
            }
            return 0;
        case XKB_KEY_i:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL)) {
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace > 2) {
                    switch_workspace(output, output->workspace - 3);
                }
                break;
            }
            return 0;
        case XKB_KEY_E:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL | WLR_MODIFIER_SHIFT)) {
                //TODO account for multiple outputs
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace < WORKSPACES - 3) {
                    move_view_to_workspace(output, output->workspace + 3);
                    switch_workspace(output, output->workspace + 3);
                }
                break;
            }
            return 0;
        case XKB_KEY_I:
            if (modifiers == (WLR_MODIFIER_LOGO | WLR_MODIFIER_CTRL | WLR_MODIFIER_SHIFT)) {
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (output->workspace > 2) {
                    move_view_to_workspace(output, output->workspace - 3);
                    switch_workspace(output, output->workspace - 3);
                }
                break;
            }
            return 0;
        case XKB_KEY_Return:
            if (modifiers == WLR_MODIFIER_LOGO) {
                if (fork() == 0) {
                    cmd("kitty");
                }
                break;
            }
            return 0;
        case XKB_KEY_semicolon:
            if (modifiers == WLR_MODIFIER_LOGO) {
                if (fork() == 0) {
                    cmd("kitty");
                }
                break;
            }
            return 0;
        case XKB_KEY_f:
            if (modifiers == WLR_MODIFIER_LOGO) {
                struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
                if (server->workspaces[output->workspace].focused_view) {
                    toggle_fullscreen(server->workspaces[output->workspace].focused_view, true);
                    break;
                }
            }
            return 0;
        case XKB_KEY_h:
            if (modifiers == WLR_MODIFIER_ALT) {
                system("mpc toggle");
                break;
            }
            return 0;
        case XKB_KEY_period:
            if (modifiers == WLR_MODIFIER_ALT) {
                system("mpc next");
                break;
            }
            return 0;
        case XKB_KEY_comma:
            if (modifiers == WLR_MODIFIER_ALT) {
                system("mpc prev");
                break;
            }
            return 0;
        case XKB_KEY_p:
            if (modifiers == WLR_MODIFIER_ALT) {
                if (fork() == 0) {
                    cmd("dbus-launch wofi --show drun");
                }
                break;
            }
            return 0;
        default:
            return 0;
    }
    return 1;
#endif
}

static void keyboard_handle_key(struct wl_listener *listener, void *data) {
    struct waytile_keyboard *keyboard = wl_container_of(listener, keyboard, key);
    struct waytile_server *server = keyboard->server;
    struct wlr_event_keyboard_key *event = data;
    struct wlr_seat *seat = server->seat;

    uint32_t keycode = event->keycode +8;
    const xkb_keysym_t sym = xkb_state_key_get_one_sym(keyboard->device->keyboard->xkb_state, keycode);

    uint32_t modifiers = wlr_keyboard_get_modifiers(keyboard->device->keyboard);
    if (event ->state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        if (handle_keybinds(sym, modifiers, server)) {
            return;
        }
    }

    wlr_seat_set_keyboard(seat, keyboard->device);
    wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode, event->state);
}

static void server_new_keyboard(struct waytile_server *server, struct wlr_input_device *device) {
    struct waytile_keyboard *keyboard = calloc(1, sizeof(struct waytile_keyboard));
    keyboard->server = server;
    keyboard->device = device;

    struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    //TODO: make those variables
    struct xkb_rule_names names;
    names.rules = "evdev";
    names.model = "pc105curl";
    names.layout = "us(cmk_ed_dh)";
    names.variant = "";
    names.options = "caps:escape";
    struct xkb_keymap *keymap = xkb_keymap_new_from_names(context, &names, XKB_KEYMAP_COMPILE_NO_FLAGS);

    wlr_keyboard_set_keymap(device->keyboard, keymap);
    xkb_keymap_unref(keymap);
    xkb_context_unref(context);
    //TODO: make those variables
    wlr_keyboard_set_repeat_info(device->keyboard, 25, 600);

    keyboard->key.notify = keyboard_handle_key;
    wl_signal_add(&device->keyboard->events.key, &keyboard->key);

    keyboard->modifiers.notify = keyboard_handle_modifiers;
    wl_signal_add(&device->keyboard->events.modifiers, &keyboard->modifiers);

    wlr_seat_set_keyboard(server->seat, device);

    wl_list_insert(&server->keyboards, &keyboard->link);
}

static void seat_request_cursor(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, request_cursor);
    struct wlr_seat_pointer_request_set_cursor_event *event = data;
    struct wlr_seat_client *focused_client = server->seat->pointer_state.focused_client;

    if (focused_client == event->seat_client) {
        wlr_cursor_set_surface(server->cursor, event->surface, event->hotspot_x, event->hotspot_y);
    }
}

static void seat_request_set_selection(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, request_set_selection);
    struct wlr_seat_request_set_selection_event *event = data;
    wlr_seat_set_selection(server->seat, event->source, event->serial);
}

static void server_new_pointer(struct waytile_server *server, struct wlr_input_device *device) {
    struct libinput_device *libdev = wlr_libinput_get_device_handle(device);
    if (libdev) {
        libinput_device_config_accel_set_profile(libdev, server->config->accel_profile);
    }
    wlr_cursor_attach_input_device(server->cursor, device);
}

static void process_cursor_motion(struct waytile_server *server, uint32_t time) {
    // TODO: add move and resize events
    double sx, sy;
    struct wlr_seat *seat = server->seat;
    struct wlr_surface *surface = NULL;
    struct waytile_view *view = desktop_view_at(server, server->cursor->x, server->cursor->y, &surface, &sx, &sy);
    if (!view) {
        wlr_xcursor_manager_set_cursor_image(server->cursor_mgr, "left_ptr", server->cursor);
    }
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);

    if (surface) {
        if (server->layer_exclusive) {
            return;
        }
        if (server->workspaces[output->workspace].fullscreen) {
            wlr_seat_pointer_notify_enter(seat, server->workspaces[output->workspace].focused_view->xdg_surface->surface, sx, sy);
            wlr_seat_pointer_notify_motion(seat, time, server->cursor->x, server->cursor->y);
        } else if (&server->layer_views_top != server->layer_views_top.next) {
            struct waytile_view *view = wl_container_of(server->layer_views_top.next, view, link);
            wlr_seat_pointer_notify_enter(seat, view->layer_surface->surface, sx, sy);
            wlr_seat_pointer_notify_motion(seat, time, server->cursor->x, server->cursor->y);
        } else {
            focus_view(view);
            wlr_seat_pointer_notify_enter(seat, surface, sx, sy);
            wlr_seat_pointer_notify_motion(seat, time, sx, sy);
        }
    } else {
        wlr_seat_pointer_clear_focus(seat);
    }
}

static void server_cursor_motion(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, cursor_motion);
    struct wlr_event_pointer_motion *event = data;
    wlr_cursor_move(server->cursor, event->device, event->delta_x, event->delta_y);
    process_cursor_motion(server, event->time_msec);
}

static void server_cursor_motion_absolute(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, cursor_motion_absolute);
    struct wlr_event_pointer_motion_absolute *event = data;
    wlr_cursor_warp_absolute(server->cursor, event->device, event->x, event->y);
    process_cursor_motion(server, event->time_msec);
}

static void server_cursor_button(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, cursor_button);
    struct wlr_event_pointer_button *event = data;
    wlr_seat_pointer_notify_button(server->seat, event->time_msec, event->button, event->state);
}

static void server_cursor_axis(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, cursor_axis);
    struct wlr_event_pointer_axis *event = data;
    wlr_seat_pointer_notify_axis(server->seat, event->time_msec, event->orientation, event->delta, event->delta_discrete, event->source);
}

static void server_cursor_frame(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, cursor_frame);
    wlr_seat_pointer_notify_frame(server->seat);
}

static void server_new_input(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, new_input);
    struct wlr_input_device *device = data;

    switch (device->type) {
        case WLR_INPUT_DEVICE_KEYBOARD:
            server_new_keyboard(server, device);
            break;
        case WLR_INPUT_DEVICE_POINTER:
            server_new_pointer(server, device);
            break;
        default:
            break;
    }

    uint32_t caps = WL_SEAT_CAPABILITY_POINTER;
    if (!wl_list_empty(&server->keyboards)) {
        caps |= WL_SEAT_CAPABILITY_KEYBOARD;
    }
    wlr_seat_set_capabilities(server->seat, caps);
}

static void server_new_output(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, new_output);
    struct wlr_output *wlr_output = data;

	wlr_output_init_render(wlr_output, server->allocator, server->renderer);

    if (!wl_list_empty(&wlr_output->modes)) {
        struct wlr_output_mode *mode = wlr_output_preferred_mode(wlr_output);
        wlr_output_set_mode(wlr_output, mode);
        wlr_output_enable(wlr_output, true);
        if (!wlr_output_commit(wlr_output)) {
            return;
        }
    }

    struct waytile_output *output = calloc(1, sizeof(struct waytile_output));
    output->wlr_output = wlr_output;
    output->server = server;

    output->frame.notify = output_frame;
    wl_signal_add(&wlr_output->events.frame, &output->frame);

    wl_list_insert(&server->outputs, &output->link);

    wlr_output_layout_add_auto(server->output_layout, wlr_output);
}

static void xdg_surface_map(struct wl_listener *listener, void *data) {
    struct waytile_view *view = wl_container_of(listener, view, map);
    struct waytile_output *output = wl_container_of(view->server->outputs.prev, output, link);
    view->mapped = 1;
    if (!output->server->workspaces[output->workspace].fullscreen && view != output->server->workspaces[output->workspace].focused_view) {
        focus_view(view);
        compute_layout(view->server);
    }
}

static void xdg_surface_unmap(struct wl_listener *listener, void *data) {
    struct waytile_view *view = wl_container_of(listener, view, unmap);
    struct waytile_output *output = wl_container_of(view->server->outputs.prev, output, link);
    view->mapped = 0;
    compute_layout(view->server);
    if (!output->server->workspaces[output->workspace].fullscreen) {
        focus_last(view);
    } else if (view == output->server->workspaces[output->workspace].focused_view) {
        focus_last(view);
        toggle_fullscreen(output->server->workspaces[output->workspace].focused_view, true);
    }
}

static void xdg_surface_destroy(struct wl_listener *listener, void *data) {
    struct waytile_view *view_cur = wl_container_of(listener, view_cur, destroy);
    struct waytile_view *view;
    wl_list_remove(&view_cur->link);
    free(view_cur);
}

static void handle_fullscreen_request(struct wl_listener *listener, void *data) {
    struct waytile_view *view = wl_container_of(listener, view, request_fullscreen);
    struct waytile_output *output = wl_container_of(view->server->outputs.prev, output, link);
    if (view->server->workspaces[output->workspace].fullscreen) {
        return;
    }
    toggle_fullscreen(view, false);
}

static void server_new_xdg_surface(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, new_xdg_surface);
    struct wlr_xdg_surface * xdg_surface = data;
    if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) {
        return;
    }
    struct waytile_view *view = calloc(1, sizeof(struct waytile_view));
    view->server = server;
    view->xdg_surface = xdg_surface;
    view->mapped = 0;

    view->map.notify = xdg_surface_map;
    wl_signal_add(&xdg_surface->events.map, &view->map);
    view->unmap.notify = xdg_surface_unmap;
    wl_signal_add(&xdg_surface->events.unmap, &view->unmap);
    view->destroy.notify = xdg_surface_destroy;
    wl_signal_add(&xdg_surface->events.destroy, &view->destroy);
    //view->request_fullscreen.notify = handle_fullscreen_request;
    //wl_signal_add(&xdg_surface->toplevel->events.request_fullscreen, &view->request_fullscreen);

    //TODO support multiple outputs
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
    wl_list_insert(&server->workspaces[output->workspace].views, &view->link);
}

static void server_new_xdg_decoration(struct wl_listener *listener, void *data) {
    struct waytile_server *server = wl_container_of(listener, server, new_decoration);
    struct wlr_xdg_toplevel_decoration_v1 *decoration = data;
    wlr_xdg_toplevel_decoration_v1_set_mode(decoration, WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
}


static void handle_output_layout_change(struct wl_listener *listener, void *data) {
    printf("this is a test\n");
    struct waytile_server *server = wl_container_of(listener, server, output_layout_change);
    struct wlr_output_configuration_v1 *config = wlr_output_configuration_v1_create();
    struct waytile_output *output;
    wl_list_for_each(output, &server->outputs, link) {
        wlr_output_configuration_head_v1_create(config, output->wlr_output);
    }
    wlr_output_manager_v1_set_configuration(server->output_manager, config);

}

int main() {
    //wlr_log_init(WLR_DEBUG, NULL);
    struct waytile_server server;

    server.wl_display = wl_display_create();
    assert(server.wl_display);

    server.backend = wlr_backend_autocreate(server.wl_display);
    assert(server.backend);

    server.renderer = wlr_renderer_autocreate(server.backend);
    wlr_renderer_init_wl_display(server.renderer, server.wl_display);

	server.allocator = wlr_allocator_autocreate(server.backend, server.renderer);

    wlr_compositor_create(server.wl_display, server.renderer);
    wlr_data_device_manager_create(server.wl_display);

    server.output_layout = wlr_output_layout_create();
    server.output_layout_change.notify = handle_output_layout_change;
    wl_signal_add(&server.output_layout->events.change, &server.output_layout_change);

    wl_list_init(&server.outputs);
    server.new_output.notify = server_new_output;
    wl_signal_add(&server.backend->events.new_output, &server.new_output);

    server.xdg_shell = wlr_xdg_shell_create(server.wl_display);
    server.new_xdg_surface.notify = server_new_xdg_surface;
    wl_signal_add(&server.xdg_shell->events.new_surface, &server.new_xdg_surface);

    server.cursor = wlr_cursor_create();
    wlr_cursor_attach_output_layout(server.cursor, server.output_layout);

    server.cursor_mgr = wlr_xcursor_manager_create(NULL, 24);
    wlr_xcursor_manager_load(server.cursor_mgr, 1);

    server.cursor_motion.notify = server_cursor_motion;
    wl_signal_add(&server.cursor->events.motion, &server.cursor_motion);
    server.cursor_motion_absolute.notify = server_cursor_motion_absolute;
    wl_signal_add(&server.cursor->events.motion_absolute, &server.cursor_motion_absolute);
    server.cursor_button.notify = server_cursor_button;
    wl_signal_add(&server.cursor->events.button, &server.cursor_button);
    server.cursor_axis.notify = server_cursor_axis;
    wl_signal_add(&server.cursor->events.axis, &server.cursor_axis);
    server.cursor_frame.notify = server_cursor_frame;
    wl_signal_add(&server.cursor->events.frame, &server.cursor_frame);

    wl_list_init(&server.keyboards);
    server.new_input.notify = server_new_input;
    wl_signal_add(&server.backend->events.new_input, &server.new_input);

    server.seat = wlr_seat_create(server.wl_display, "seat0");
    assert(server.seat);

    server.request_cursor.notify = seat_request_cursor;
    wl_signal_add(&server.seat->events.request_set_cursor, &server.request_cursor);
    server.request_set_selection.notify = seat_request_set_selection;
    wl_signal_add(&server.seat->events.request_set_selection, &server.request_set_selection);

    server.decoration_manager = wlr_xdg_decoration_manager_v1_create(server.wl_display);
    server.dec_man = wlr_server_decoration_manager_create(server.wl_display);
    wlr_server_decoration_manager_set_default_mode(server.dec_man, WLR_SERVER_DECORATION_MANAGER_MODE_SERVER);

    server.new_decoration.notify = server_new_xdg_decoration;
    wl_signal_add(&server.decoration_manager->events.new_toplevel_decoration, &server.new_decoration);

    layer_shell_init(&server);

    output_manager_init(&server);

    struct wlr_xdg_output_manager_v1 *man;
    struct wlr_xdg_output_v1 *put;
    man = wlr_xdg_output_manager_v1_create(server.wl_display, server.output_layout);

    wl_list_for_each(put, &man->outputs, link) {
        printf("%ix%i amougs", put->width, put->height);
    }
    workspaces_init(&server);

    wlr_screencopy_manager_v1_create(server.wl_display);
    wlr_primary_selection_v1_device_manager_create(server.wl_display);
    wlr_gamma_control_manager_v1_create(server.wl_display);
    wlr_data_control_manager_v1_create(server.wl_display);

    server.config = malloc(sizeof(struct waytile_config));
    server.config->background_color[0] = 0.3;
    server.config->background_color[1] = 0.3;
    server.config->background_color[2] = 0.3;
    server.config->background_color[3] = 1.;
	server.config->border_thickness = 5;
    server.config->border_active_color[0] = 0.1;
    server.config->border_active_color[1] = 0.7;
    server.config->border_active_color[2] = 1.;
    server.config->border_active_color[3] = 1.;
    server.config->border_inactive_color[0] = 0.1;
    server.config->border_inactive_color[1] = 0.3;
    server.config->border_inactive_color[2] = 0.7;
    server.config->border_inactive_color[3] = 1.;
    server.config->gap_x = 10 + server.config->border_thickness;
    server.config->gap_y = 10 + server.config->border_thickness;
    server.config->gap_x_inner = 10 + server.config->border_thickness * 2;
    server.config->gap_y_inner = 10 + server.config->border_thickness * 2;
    server.config->accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT;

    server.layer_exclusive = 0;

    const char *socket = wl_display_add_socket_auto(server.wl_display);

#if !DEBUG
    if (!fork()) {
        cmd("sh /home/robin/.config/dwl/autostart.sh");
    }
#endif
    if (!socket) {
        wlr_backend_destroy(server.backend);
        return 1;
    }

    if (!wlr_backend_start(server.backend)) {
        fprintf(stderr, "failed to start backend, L\n");
        wlr_backend_destroy(server.backend);
        wl_display_destroy(server.wl_display);
        return 1;
    }

    setenv("WAYLAND_DISPLAY", socket, true);

    wl_display_run(server.wl_display);

    wl_display_destroy_clients(server.wl_display);
    wl_display_destroy(server.wl_display);
    return 0;
}
