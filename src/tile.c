static void compute_layout_on_workspace(struct waytile_server *server, int workspace) {
    int width, height;
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
    wlr_output_effective_resolution(output->wlr_output, &width, &height);
    width -= server->config->gap_x * 2;
    height -= server->config->gap_y * 2;
    struct waytile_view *view;
    int length = 0, pos = 0;
    wl_list_for_each(view, &server->workspaces[workspace].views, link) {
        if (view->mapped) {
            length++;
        }
    }
    width -= server->config->gap_x_inner * (length-1);
    wl_list_for_each(view, &server->workspaces[workspace].views, link) {
        if (view->mapped) {
            view->width = width / length;
            view->height = height;
            set_size(view);
            view->x = (width / length + server->config->gap_x_inner) * pos;
            view->x += server->config->gap_x;
            view->y = 0;
            view->y += server->config->gap_y;
            pos++;
        }
    }
}

static void compute_layout(struct waytile_server *server) {
    //TODO multiple outputs
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
    compute_layout_on_workspace(server, output->workspace);
}
