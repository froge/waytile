struct plugin {
    struct waytile_server *server;
    struct wl_list link;
    void *func;
    void *data;
};

struct waytile_server {
    struct wl_display *wl_display;
    struct wl_event_loop *wl_event_loop;

    struct wlr_backend *backend;
    struct wlr_renderer *renderer;
	struct wlr_allocator *allocator;

    struct wlr_output_layout *output_layout;
    struct wl_listener new_output;
    struct wl_listener output_layout_change;
    struct wl_list outputs;
    struct wlr_output_manager_v1 *output_manager;
    struct wlr_xdg_output_manager_v1 *xdg_output_manager;

    struct wlr_seat *seat;
    struct wl_listener new_input;
    struct wl_list keyboards;

    struct wlr_cursor *cursor;
    struct wlr_xcursor_manager *cursor_mgr;
	struct wl_listener cursor_motion;
	struct wl_listener cursor_motion_absolute;
	struct wl_listener cursor_button;
	struct wl_listener cursor_axis;
	struct wl_listener cursor_frame;

    struct wl_listener request_cursor;
    struct wl_listener request_set_selection;

    struct wlr_xdg_decoration_manager_v1 *decoration_manager;
    struct wl_listener new_decoration;

    struct wlr_server_decoration_manager *dec_man;

    struct wlr_xdg_shell *xdg_shell;
    struct wl_listener new_xdg_surface;
    struct wl_listener destroy_xdg_surface;

    struct wlr_layer_shell_v1 *layer_shell;
    struct wlr_layer_surface_v1 *layer_exclusive;
    struct wl_list layer_views_background;
    struct wl_list layer_views_bottom;
    struct wl_list layer_views_top;
    struct wl_list layer_views_overlay;
    struct wl_listener new_layer_surface;

    struct workspace *workspaces;

    struct waytile_config *config;

    struct wl_list pre_surface;
    struct wl_list post_surface;
};

struct waytile_output {
    struct wl_list link;

    struct wlr_output *wlr_output;
    int workspace;
    struct waytile_server *server;

    struct wl_listener destroy;
    struct wl_listener frame;
};

struct waytile_keyboard {
    struct wl_list link;

    struct waytile_server *server;
    struct wlr_input_device *device;

    struct wl_listener modifiers;
    struct wl_listener key;
};

struct waytile_view {
    struct wl_list link;

    struct waytile_server *server;

    struct workspace *workspace;

    struct wlr_xdg_surface *xdg_surface;
    struct wlr_layer_surface_v1 *layer_surface;

    struct wl_listener map;
    struct wl_listener unmap;
    struct wl_listener destroy;

    struct wl_listener request_fullscreen;

    int mapped;
    int x, y;
    int width, height;
};

struct workspace {
    struct wl_list views;
    struct waytile_view *focused_view;
    struct waytile_output *output;
    bool fullscreen;
};

struct waytile_config {
    float background_color[4];
    int gap_x;
    int gap_y;
    int gap_x_inner;
    int gap_y_inner;
    enum libinput_config_accel_profile accel_profile;
	int border_thickness;
	float border_active_color[4];
	float border_inactive_color[4];
};
