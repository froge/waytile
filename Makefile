PKGS = wlroots wayland-server xcb xkbcommon libinput

CFLAGS += -DWLR_USE_UNSTABLE -Iprotocols -g

WAYLAND_PROTOCOLS=$(shell pkg-config --variable=pkgdatadir wayland-protocols)


CFLAGS += $(foreach p,$(PKGS),$(shell pkg-config --cflags $(p)))
LDLIBS += $(foreach p,$(PKGS),$(shell pkg-config --libs $(p))) -lm

all: protocols/wlr-layer-shell-unstable-v1-protocol.c protocols/wlr-layer-shell-unstable-v1-protocol.h protocols/xdg-shell-protocol.h protocols/xdg-shell-protocol.c release
	gcc src/main.c -o waytile $(CFLAGS) $(LDLIBS) -DDEBUG

run: all
	./waytile

release:
	gcc src/main.c -o waytile $(CFLAGS) $(LDLIBS)
	cp -f waytile waytile_release

test: all
	./lang > test

protocols/xdg-shell-protocol.h:
	wayland-scanner server-header \
		$(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

protocols/xdg-shell-protocol.c:
	wayland-scanner private-code \
		$(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

protocols/wlr-layer-shell-unstable-v1-protocol.h:
	wayland-scanner server-header \
		protocols/wlr-layer-shell-unstable-v1.xml $@

protocols/wlr-layer-shell-unstable-v1-protocol.c:
	wayland-scanner private-code \
		protocols/wlr-layer-shell-unstable-v1.xml $@

#amongus
