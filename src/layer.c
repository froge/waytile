static void layer_surface_map(struct wl_listener *listener, void *data) {
    struct waytile_view *view = wl_container_of(listener, view, map);
    view->mapped = 1;
    if (view->layer_surface->current.keyboard_interactive == 1) {
        view->server->layer_exclusive = view->layer_surface;
        focus_view(view);
    }
}

static void layer_surface_unmap(struct wl_listener *listener, void *data) {
    struct waytile_view *view = wl_container_of(listener, view, unmap);
    view->mapped = 0;
    if (view->server->layer_exclusive == view->layer_surface) {
        view->server->layer_exclusive = 0;
    }
    focus_last(view);
}

static void layer_surface_destroy(struct wl_listener *listener, void *data) {
    struct waytile_view *view = wl_container_of(listener, view, destroy);
    wl_list_remove(&view->link);
    free(view);
}

static void server_new_layer_surface(struct wl_listener *listener, void *data) {
    printf("amogus");
    struct waytile_server *server = wl_container_of(listener, server, new_layer_surface);
    struct wlr_layer_surface_v1 *surface = data;
    struct waytile_view *view = calloc(1, sizeof(struct waytile_view));
    view->server = server;
    view->layer_surface = surface;
    view->mapped = 0;
    //TODO account for multiple outputs
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);
    if (!surface->output) {
        surface->output = output->wlr_output;
    }

    view->map.notify = layer_surface_map;
    wl_signal_add(&surface->events.map, &view->map);
    view->unmap.notify = layer_surface_unmap;
    wl_signal_add(&surface->events.unmap, &view->unmap);
    view->destroy.notify = layer_surface_destroy;
    wl_signal_add(&surface->events.destroy, &view->destroy);

    view->x = surface->current.desired_width;
    view->y = surface->current.desired_height;
    int width, height;
    wlr_output_effective_resolution(output->wlr_output, &width, &height);
    if (surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP &&
        surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM) {
    } else if (surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP) {
        view->y = 0 + surface->current.margin.top;
    } else if (surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM) {
        view->y = height - surface->current.desired_height - surface->current.margin.bottom;
    }
    if (surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT &&
        surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT) {
    } else if (surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT) {
        view->x = 0 + surface->current.margin.left;
    } else if (surface->current.anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT) {
        view->x = width - surface->current.desired_width - surface->current.margin.right;
    }
    view->width = width - view->x - surface->current.margin.right;
    view->height = height - view->y - surface->current.margin.bottom;
    wlr_layer_surface_v1_configure(surface, view->width, view->height);

    printf("among ;%i, %i\n", view->x, view->y);
    printf("among ;%i, %i\n", view->width, view->height);
    printf("%i\n", surface->current.anchor);

    switch (surface->current.layer) {
        case ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND:
            wl_list_insert(&server->layer_views_background, &view->link);
            break;
        case ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM:
            wl_list_insert(&server->layer_views_bottom, &view->link);
            break;
        case ZWLR_LAYER_SHELL_V1_LAYER_TOP:
            wl_list_insert(&server->layer_views_top, &view->link);
            break;
        case ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY:
            wl_list_insert(&server->layer_views_overlay, &view->link);
            break;
    }
}

static void layer_shell_init(struct waytile_server *server) {
    server->layer_shell = wlr_layer_shell_v1_create(server->wl_display);
    server->new_layer_surface.notify = server_new_layer_surface;
    wl_signal_add(&server->layer_shell->events.new_surface, &server->new_layer_surface);
    wl_list_init(&server->layer_views_background);
    wl_list_init(&server->layer_views_bottom);
    wl_list_init(&server->layer_views_top);
    wl_list_init(&server->layer_views_overlay);
}
