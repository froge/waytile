static void output_manager_init(struct waytile_server *server) {
    server->output_manager = wlr_output_manager_v1_create(server->wl_display);
    struct waytile_output *output;
    struct wlr_output_configuration_v1 *config = wlr_output_configuration_v1_create();
    wl_list_for_each(output, &server->outputs, link) {
        wlr_output_configuration_head_v1_create(config, output->wlr_output);
    }
    wlr_output_manager_v1_set_configuration(server->output_manager, config);
}
