static void workspaces_init(struct waytile_server *server) {
    server->workspaces = malloc(sizeof(struct workspace) * WORKSPACES);
    struct workspace *workspace;
    struct waytile_output *output = wl_container_of(server->outputs.prev, output, link);;
    for (int i = 0; i < WORKSPACES; i++) {
        workspace = &server->workspaces[i];
        workspace->fullscreen = 0;
        wl_list_init(&workspace->views);
        workspace->focused_view = NULL;
        workspace->output = output;
    }
}

static void switch_workspace(struct waytile_output *output, int workspace) {
    output->workspace = workspace;
    focus_view(output->server->workspaces[workspace].focused_view);
}

static void move_view_to_workspace(struct waytile_output *output, int workspace_i) {
     //TODO: fix this bs :)
    struct workspace *workspace = &output->server->workspaces[output->workspace];
    struct workspace *workspace_next = &output->server->workspaces[workspace_i];
    struct waytile_view *view = workspace->focused_view;
    wl_list_remove(&view->link);
    wl_list_insert(workspace_next->views.prev, &view->link);
    focus_last(workspace->focused_view);
    compute_layout_on_workspace(output->server, workspace_i);
    compute_layout(output->server);
    workspace_next->focused_view = view;
}
