static void set_size(struct waytile_view *view) {
    if (view->xdg_surface) {
        wlr_xdg_toplevel_set_size(view->xdg_surface, view->width, view->height);
    }
}

static void focus_view(struct waytile_view *view) {
    assert(view);
    //TODO: account for multiple outputs
    struct waytile_output *output = wl_container_of(view->server->outputs.prev, output, link);
    struct waytile_server *server = view->server;
    struct wlr_seat *seat = server->seat;
    struct wlr_surface *prev_surface = seat->keyboard_state.focused_surface;
    struct wlr_surface *surface;
    if (view->xdg_surface) {
        surface = view->xdg_surface->surface;
        if (prev_surface == surface)
            return;

        if (prev_surface) {
            if (wlr_surface_is_xdg_surface(seat->keyboard_state.focused_surface)) {
                struct wlr_xdg_surface *previous = wlr_xdg_surface_from_wlr_surface(seat->keyboard_state.focused_surface);
                wlr_xdg_toplevel_set_activated(previous, false);
            }
        }
        wlr_xdg_toplevel_set_activated(view->xdg_surface, true);
    }
    if (view->layer_surface) {
        surface = view->layer_surface->surface;
        if (prev_surface == surface)
            return;
        if (prev_surface) {
            struct wlr_xdg_surface *previous = wlr_xdg_surface_from_wlr_surface(seat->keyboard_state.focused_surface);
            wlr_xdg_toplevel_set_activated(previous, false);
        }
    }
    struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(seat);
    wlr_seat_keyboard_notify_enter(seat, surface, keyboard->keycodes, keyboard->num_keycodes, &keyboard->modifiers);
    //wl_list_remove(&view->link);
    //wl_list_insert(server->views.prev, &view->link);
    server->workspaces[output->workspace].focused_view = view;
    assert(seat->keyboard_state.focused_surface == surface);
}

#include "tile.c"

static void toggle_fullscreen(struct waytile_view *view, bool amog) {
    struct waytile_output *output = wl_container_of(view->server->outputs.prev, output, link);
    int width, height;
    wlr_output_effective_resolution(output->wlr_output, &width, &height);
    //TODO add other types
    if (output->server->workspaces[output->workspace].fullscreen) {
        output->server->workspaces[output->workspace].fullscreen = false;
        if (amog) {
            wlr_xdg_toplevel_set_fullscreen(view->xdg_surface, false);
        }
        compute_layout(view->server);
    } else {
        output->server->workspaces[output->workspace].fullscreen = true;
        if (amog) {
            wlr_xdg_toplevel_set_fullscreen(view->xdg_surface, true);
        }
        view->x = 0;
        view->y = 0;
        view->width = width;
        view->height = height;
        set_size(view);
    }
}

static void focus_last(struct waytile_view *cur_view) {
    struct waytile_view *view;
    //TODO multiple outputs
    struct waytile_output *output = wl_container_of(cur_view->server->outputs.prev, output, link);
    if (wl_list_length(&cur_view->server->workspaces[output->workspace].views) > 1) {
        wl_list_for_each(view, &cur_view->server->workspaces[output->workspace].views, link) {
            if (view->mapped && view != cur_view) {
                if (!view->xdg_surface) {
                    continue;
                }
                focus_view(view);
                return;
            }
        }
    }
    cur_view->server->workspaces[output->workspace].focused_view = NULL;
}

